package gobdd

func Collect(arr []int) map[string]int {
  out := make(map[string]int)
  min, max, count, sum := arr[0], arr[0], len(arr), 0

  for _, v := range arr {

    if v < min {
      min = v
    }

    if v > max {
      max = v
    }

    sum = sum + v
  }

  out["min"] = min
  out["max"] = max
  out["count"] = count
  out["sum"] = sum
  out["avg"] = sum / count

  return out
}
