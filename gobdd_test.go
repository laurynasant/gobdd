package gobdd_test

import (
	. "bitbucket.org/laurynasant/gobdd"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Gobdd", func() {
	var result map[string]int
	BeforeEach(func() {
	  input := []int{3, 3, 4, 5, 7, 99, -99, 7, 8, 64}
	  result = Collect(input)
	})

	It("collects minimum value", func() {
	  Expect(result["min"]).To(Equal(-99))
	})

	It("collects maximum value", func() {
		Expect(result["max"]).To(Equal(99))
	})

	It("collects number of elements", func() {
		Expect(result["count"]).To(Equal(10))
	})

	It("collects sum of elements", func() {
		Expect(result["sum"]).To(Equal(101))
	})

	It("collects average value", func() {
		Expect(result["avg"]).To(Equal(10))
	})
})
